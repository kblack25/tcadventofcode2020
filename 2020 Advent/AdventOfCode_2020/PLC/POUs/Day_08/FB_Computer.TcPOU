﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.4">
  <POU Name="FB_Computer" Id="{ac284e83-ca4a-4932-bdeb-c46350ef7677}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK PUBLIC FB_Computer
VAR_IN_OUT
	InstructionSet		: ARRAY[0..GVL.ARRAY_END] OF STRING;
END_VAR
VAR
	_Accumulator		: INT;
	_InstructionIndex	: INT;
	_Instruction		: STRING(3);
	_Value				: INT;
	_BlockParsing		: BOOL;
	_BranchIndex		: INT;
	_CatalogMade		: BOOL;
	_CatalogIndex		: INT;
	_Catalog			: ARRAY[0..GVL.ARRAY_END] OF ST_InstructionLog;
	_BreadCrumbTrail	: ARRAY[0..GVL.ARRAY_END] OF INT;
	_Halt				: BOOL;
	_Error				: BOOL;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[IF _BreadCrumbTrail[_InstructionIndex] > 0 AND NOT(_Halt OR _Error) THEN
	_CatalogMade := TRUE;
	M_RevertAndFork(_BranchIndex);
	_BranchIndex := _BranchIndex + 1;
END_IF

IF NOT(_Halt OR _Error) THEN
	_BreadCrumbTrail[_InstructionIndex] := _BreadCrumbTrail[_InstructionIndex] + 1;
	IF NOT _BlockParsing THEN
		M_ParseInstruction(Instruction := InstructionSet[_InstructionIndex]);
	END_IF
	_BlockParsing := FALSE;
	IF _Instruction = 'acc' THEN
		M_Acc(Value := _Value);
	ELSIF _Instruction = 'jmp' THEN
		IF NOT _CatalogMade THEN
			M_LogInstruction();
		END_IF
		M_Jmp(Value := _Value);
	ELSIF _Instruction = 'nop' THEN
		IF NOT _CatalogMade THEN
			M_LogInstruction();
		END_IF
		M_Nop();
	ELSE
		_Halt := TRUE;
	END_IF
END_IF]]></ST>
    </Implementation>
    <Method Name="M_Acc" Id="{46babd7b-70e9-4aab-bc54-4a6c5b757dc7}">
      <Declaration><![CDATA[METHOD PRIVATE M_Acc
VAR_INPUT
	Value	: INT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_Accumulator := _Accumulator + Value;
_InstructionIndex := _InstructionIndex + 1;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_ClearAll" Id="{0a750096-7081-4e97-a653-743e08efef4f}">
      <Declaration><![CDATA[METHOD PUBLIC M_ClearAll
VAR
	i	: INT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[_Accumulator := 0;
_InstructionIndex := 0;
_BranchIndex := 0;
_Value := 0;
_Instruction := '';
_Halt:= FALSE;
_CatalogMade := FALSE;
_CatalogIndex := 0;
_Error := FALSE;
FOR i := 0 TO GVL.ARRAY_END DO
	_BreadCrumbTrail[i] := 0;
	_Catalog[i] := GVL.EMPTY_INSTRUCTION_LOG;
END_FOR]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Jmp" Id="{33966333-e0c4-41f5-ba0e-c05dc78a3e0f}">
      <Declaration><![CDATA[METHOD PRIVATE M_Jmp
VAR_INPUT
	Value	: INT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_InstructionIndex := _InstructionIndex + Value;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_LogInstruction" Id="{2285c223-cf7a-416b-85f5-8d57106b219c}">
      <Declaration><![CDATA[METHOD INTERNAL M_LogInstruction
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_Catalog[_CatalogIndex].Accumulator := _Accumulator;
_Catalog[_CatalogIndex].Index := _InstructionIndex;
_Catalog[_CatalogIndex].Instruction := _Instruction;
_Catalog[_CatalogIndex].Value := _Value;
_CatalogIndex := _CatalogIndex + 1;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Nop" Id="{e7546bc9-b776-4b86-8e7d-7aab90a5fc33}">
      <Declaration><![CDATA[METHOD PRIVATE M_Nop

]]></Declaration>
      <Implementation>
        <ST><![CDATA[_InstructionIndex := _InstructionIndex + 1;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_ParseInstruction" Id="{ca89e3dd-435c-4e4d-b5a2-7de3d62ef711}">
      <Declaration><![CDATA[METHOD PRIVATE M_ParseInstruction
VAR_INPUT
	Instruction : STRING;
END_VAR

]]></Declaration>
      <Implementation>
        <ST><![CDATA[_Instruction := LEFT(STR := Instruction, SIZE := 3);
_Value := STRING_TO_INT(MID(STR := Instruction, LEN := LEN(Instruction) - 5, POS := 5));]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_RevertAndFork" Id="{7fa3988c-0590-4fda-872d-34374716d08a}">
      <Declaration><![CDATA[METHOD INTERNAL M_RevertAndFork
VAR_INPUT
	BranchIndex	: INT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[_Accumulator := _Catalog[BranchIndex].Accumulator;
_InstructionIndex := _Catalog[BranchIndex].Index;
_Value := _Catalog[BranchIndex].Value;

IF _Catalog[BranchIndex].Instruction = 'jmp' THEN
	_Instruction := 'nop';
ELSIF _Catalog[BranchIndex].Instruction = 'nop' THEN
	_Instruction := 'jmp';	
ELSE
	_Error := TRUE;
END_IF

_Catalog[BranchIndex].ForkTested := TRUE;
_BlockParsing := TRUE;
]]></ST>
      </Implementation>
    </Method>
    <Property Name="P_Accumulator" Id="{c7b26422-fc44-4875-a6ef-fbc5a55e4023}">
      <Declaration><![CDATA[PROPERTY PUBLIC P_Accumulator : INT]]></Declaration>
      <Get Name="Get" Id="{5ce2ecda-f6cb-499b-8f46-fbd955962fa2}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[P_Accumulator := _Accumulator;]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{321b89fb-68e3-49c4-9261-d625e62006a8}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[_Accumulator := P_Accumulator;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <LineIds Name="FB_Computer">
      <LineId Id="172" Count="26" />
      <LineId Id="22" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.M_Acc">
      <LineId Id="5" Count="0" />
      <LineId Id="8" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.M_ClearAll">
      <LineId Id="5" Count="4" />
      <LineId Id="11" Count="0" />
      <LineId Id="18" Count="2" />
      <LineId Id="12" Count="0" />
      <LineId Id="15" Count="0" />
      <LineId Id="17" Count="0" />
      <LineId Id="16" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.M_Jmp">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.M_LogInstruction">
      <LineId Id="6" Count="2" />
      <LineId Id="5" Count="0" />
      <LineId Id="9" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.M_Nop">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.M_ParseInstruction">
      <LineId Id="5" Count="0" />
      <LineId Id="13" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.M_RevertAndFork">
      <LineId Id="29" Count="13" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.P_Accumulator.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_Computer.P_Accumulator.Set">
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>